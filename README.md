Don't use this.

It's AWFUL.

This role is designed to cause a given user's prompt to spit out a message on
every command. It is also designed to be challenging to revert, but not
destructive. This role is NOT idempotent, kinda (depending on what you mean by
that).

Inspired by https://twitter.com/Rtzq0/status/738450690914160640

## Usage

Create a playbook like so:
```
- hosts:
  - All
- roles:
  - obnoxious_prank
```

By default this will create a new file in /etc/profile.d with a random filename

obnoxious_prank supports SELinux!!

## Variables/Defaults

```
# default to being prankable
prank_host_prankable: True
# and to being extra mean
prank_be_so_mean: True

# This part is the obnoxious part. Random fileames.
prank_file_name: "{{ 10000000000000000000000|random }}.sh"

# Default text and user, calls people out when they sudo su -
prank_pranked_user: "root"
prank_echo_line: "I see you..."

# Default perms, works on RHEL 6
prank_file_mode: "0644"
prank_file_owner: "root"
prank_file_group: "root"
prank_file_seuser: "system_u"
prank_file_serole: "object_r"
prank_file_setype: "bin_t"
prank_file_selevel: "s0"
```